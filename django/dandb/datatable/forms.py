 import ModelForm, forms from models import Event, Location, EventSchedule

class EventForm(ModelForm):
    class Meta:
        model = Event
        fields = ('name','description','category','age_req','cost_per_person')

class LocationForm(ModelForm):
    class Meta:
        model = Location
        fields = ('name','city','state_code','zip','country')

class EventScheduleForm(ModelForm):
    class Meta:
        model = EventSchedule
        fields = ('start_date_time', 'end_date_time')
