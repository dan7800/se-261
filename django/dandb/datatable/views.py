import datetime
from django.shortcuts import render
from .models import Person
from django.http import HttpResponse
from django.contrib.auth import authenticate, login

def secret_page(request):
    return render(request, 'secret_page.html')

def logincheck(request):
    username = request.POST['username']
    password = request.POST['password']
  
    #username = request.GET['username']
    #password = request.GET['password'] 
    FullName='ha'
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
#	    return HttpResponse('Correct credentials.')
	   # return render(request, 'secret_page.html')

	    person = Person.objects.filter(fname__icontains='dan')
            return render(request, 'secret_page.html')

	else:
		return HttpResponse('Disabled account.')
            # Return a 'disabled account' error message
    else:
	return HttpResponse("<img src=""http://www.lgbtsr.org/wp-content/uploads/2013/05/laughing-old-lady.jpg""> </br>Invalid Login")
        # Return an 'invalid login' error message.



# Add the name information for the Person
def data(request):
        if 'lname' in request.GET and request.GET['lname']:
                lname = request.GET['lname']
		fname = request.GET['fname']
		FullName = fname + ' ' + lname

                #Add the item to the table
                new_event = Person(fname=fname,lname=lname)
                new_event.save()
 		person = Person.objects.filter(fname__icontains=fname)
                return render(request, 'data_results.html',
                {'person': person, 'query': FullName})
        else:
                return HttpResponse('Please submit a name to add.')

# Search the name information for a Person
def search(request):
	if 'q' in request.GET and request.GET['q']:
        	q = request.GET['q']

        	person = Person.objects.filter(lname__icontains=q)
        	return render(request, 'search_results.html',
            	{'person': person, 'query': q})
    	else:
        	return HttpResponse('Please submit a search term.')

def data_form(request):
    return render(request, 'data_form.html')

def search_form(request):
    return render(request, 'search_form.html')

# The default web application page. Holds a simple CRUD operation
def default(request):
	#Add dummy items to database 
	topPerson = Person.objects.all()[:1]
	allPeople = Person.objects.all()


# Update existing records
	allPeople.update(lname='Smith')


# This will add data to table
	new_event = Person(fname='fname',lname=datetime.datetime.now())
	new_event.save()

	# Remove items from table
#	allPeople.delete()

   	return render(request, "people.html", {"topperson": topPerson, "allpeople": allPeople})
