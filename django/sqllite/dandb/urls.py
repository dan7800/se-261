from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('dandb.datatable.views',
    url(r'^$','default'),
    url(r'^search_form/$','search_form'),
    url(r'^search/$','search'),
    url(r'^data_form/$','data_form'),
    url(r'^data/$','data'),
    url(r'^logincheck/$','logincheck'),

)
