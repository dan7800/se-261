from django.db import models

class Person(models.Model):
        fname=models.TextField(verbose_name="full name")
        lname=models.TextField(verbose_name="last name")
        middle=models.TextField(verbose_name="middle name")
        age=models.TextField(verbose_name="Age")
